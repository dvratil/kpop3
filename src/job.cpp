/*
    Copyright (c) 2016 Daniel Vrátil <dvrati@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "job.h"
#include "job_p.h"
#include "session_p.h"

#include "listjob.h"
#include "retrjob.h"
#include "statjob.h"
#include "capajob.h"

#include "kpop3_debug.h"

#include <QTimer>

using namespace KPOP3;

Job::Job(Session *session)
    : KJob(session)
    , d_ptr(new JobPrivate(session))
{
}

Job::Job(JobPrivate &dd)
    : KJob(dd.m_session)
    , d_ptr(&dd)
{
    QTimer::singleShot(0, this, &Job::start);
}

Job::~Job()
{
    delete d_ptr;
}

Session *Job::session() const
{
    Q_D(const Job);
    return d->m_session;
}

void Job::start()
{
    Q_D(Job);
    d->session()->addJob(this);
}

void Job::handleResponse(const QByteArray &response)
{
    handleErrorReplies(response);
}

void Job::connectionLost()
{
    setError(KJob::UserDefinedError);
    emitResult();
}

Job::HandlerResponse Job::handleErrorReplies(const QByteArray &response)
{
    setError(UserDefinedError);
    setErrorText(QString::fromUtf8(response.mid(5)));
    emitResult();
    return Handled;
}

ListJob::ListJob(Session *session)
    : Job(*new JobPrivate(session))
{}

RetrJob::RetrJob(Session *session)
    : Job(*new JobPrivate(session))
{}

StatJob::StatJob(Session *session)
    : Job(*new JobPrivate(session))
{}

CapaJob::CapaJob(Session *session)
    : Job(*new JobPrivate(session))
{}
