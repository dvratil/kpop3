/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef KPOP3_MESSAGE_H
#define KPOP3_MESSAGE_H

#include <QByteArray>
#include <QVector>

namespace KPOP3 {

using MsgID = quint32;

class Message
{
public:
    Message()
        : size(0), id(0)
    {}

    Message(MsgID id, quint64 size)
        : size(size), id(id)
    {}

    Message(MsgID id, const QByteArray &uid)
        : size(0), uid(uid), id(id)
    {}

    quint64 size;
    QByteArray uid;
    QByteArray data;
    MsgID id;
};

using MessageList = QVector<Message>;

}

#endif
