/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "connection.h"
#include "session_p.h"
#include "impl/capajobimpl_p.h"
#include "impl/delejobimpl_p.h"
#include "impl/listjobimpl_p.h"
#include "impl/loginjobimpl_p.h"
#include "impl/quitjobimpl_p.h"
#include "impl/retrjobimpl_p.h"
#include "impl/rsetjobimpl_p.h"
#include "impl/statjobimpl_p.h"

#include <QNetworkProxy>

namespace KPOP3 {

class ConnectionPrivate
{
public:
    ConnectionPrivate(Connection *parent)
        : mProxy(QNetworkProxy::NoProxy)
        , mSession(Q_NULLPTR)
        , mPort(0)
        , mCryptoMode(QSsl::AnyProtocol)
        , mUseSSL(false)
        , q_ptr(parent)
    {}

    ~ConnectionPrivate()
    {
        delete mSession;
    }

    Session *session()
    {
        Q_Q(Connection);
        if (!mSession) {
            mSession = new Session(mHost, mPort, mUseSSL, mCryptoMode, q);
            q->connect(mSession, &Session::stateChanged,
                       q, &Connection::stateChanged);
        }

        return mSession;
    }

    QNetworkProxy mProxy;
    Session *mSession;
    QString mHost;
    quint16 mPort;
    QSsl::SslProtocol mCryptoMode;
    bool mUseSSL;

private:
    Connection * const q_ptr;
    Q_DECLARE_PUBLIC(Connection)
};

}

using namespace KPOP3;

Connection::Connection(const QString &host, quint16 port, QObject *parent)
    : QObject(parent)
    , d_ptr(new ConnectionPrivate(this))
{
    Q_D(Connection);
    d->mHost = host;
    d->mPort = port;
}

Connection::Connection(const QString &host, quint16 port, QSsl::SslProtocol cryptoMode,
                       QObject *parent)
    : QObject(parent)
    , d_ptr(new ConnectionPrivate(this))
{
    Q_D(Connection);
    d->mHost = host;
    d->mPort = port;
    d->mCryptoMode = cryptoMode;
    d->mUseSSL = true;
}

Connection::~Connection()
{
    delete d_ptr;
}

ConnectionState Connection::state() const
{
    Q_D(const Connection);
    if (d->mSession) {
        return d->mSession->state();
    } else {
        return DisconnectedState;
    }
}

void Connection::setProxy(const QNetworkProxy &proxy)
{
    Q_D(Connection);
    d->mProxy = proxy;
    if (d->mSession) {
        d->mSession->setProxy(proxy);
    }
}

QNetworkProxy Connection::proxy() const
{
    return d_func()->mProxy;
}

CapaJob *Connection::capa()
{
    return new CapaJobImpl(d_func()->session());
}

ListJob *Connection::list()
{
    return new ListJobImpl(ListJobImpl::LIST, d_func()->session());
}

ListJob *Connection::list(MsgID msg)
{
    return new ListJobImpl(msg, ListJobImpl::LIST, d_func()->session());
}

ListJob *Connection::uidl()
{
    return new ListJobImpl(ListJobImpl::UIDL, d_func()->session());
}

ListJob *Connection::uidl(MsgID msg)
{
    return new ListJobImpl(msg, ListJobImpl::UIDL, d_func()->session());
}

RetrJob *Connection::retr(MsgID msg)
{
    return new RetrJobImpl(msg, d_func()->session());
}

RetrJob *Connection::top(MsgID msg, int lineCount)
{
    return new RetrJobImpl(msg, lineCount, d_func()->session());
}

Job *Connection::dele(MsgID msg)
{
    return new DeleJobImpl(msg, d_func()->session());
}

Job *Connection::rset()
{
    return new RsetJobImpl(d_func()->session());
}

StatJob *Connection::stat()
{
    return new StatJobImpl(d_func()->session());
}

Job *Connection::login(const QString &username, const QString &password,
                       AuthenticationMethod authMethod)
{
    return new LoginJobImpl(username, password, authMethod, d_func()->session());
}

Job *Connection::quit()
{
    return new QuitJobImpl(d_func()->session());
}
