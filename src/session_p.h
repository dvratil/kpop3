/*
 * Copyright (C) 2015 Daniel Vrátil <dvratil@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef KPOP3_SESSION_P_H
#define KPOP3_SESSION_P_H

#include <QObject>
#include <QSsl>
#include <QQueue>

#include <QAbstractSocket>
#include <QNetworkProxy>

#include "kpop3_export.h"
#include "enums.h"

class QString;
class KJob;
class QFile;
class QTimer;
class QTcpSocket;

namespace KPOP3 {

class SessionPrivate;
class JobPrivate;
class Job;

class KPOP3_EXPORT Session : public QObject
{
    Q_OBJECT

public:
    explicit Session(const QString &hostName, quint16 port,
                     bool useSSL, QSsl::SslProtocol proto,
                     QObject *parent = Q_NULLPTR);
    ~Session();

    void setProxy(const QNetworkProxy &proxy);

    void setState(ConnectionState state);
    ConnectionState state() const { return mState; }

    QByteArray serverGreeting() const { return mServerGreeting; }
    QString hostname() const { return mHost; }

    void addJob(Job *job);
    void startNext();

    void sendCommand(const QByteArray &command);

    void setSocketTimeout(int timeout);
    void startSocketTimer();
    void stopSocketTimer();
    void restartSocketTimer();

    void close();

Q_SIGNALS:
    void connectionFailed();

    void stateChanged(KPOP3::ConnectionState state);

private Q_SLOTS:
    void reconnect();

    void onJobFinished(KJob *job);
    void onJobDestroyed(QObject *job);
    void onReadyRead();
    void onSocketStateChanged(QAbstractSocket::SocketState state);
    void onSocketTimeout();
    void onSocketError();
    void onSocketEncrypted();

private:
    Q_DISABLE_COPY(Session)

    QNetworkProxy mProxy;
    QString mHost;
    quint16 mPort;
    bool mUseSSL;
    QSsl::SslProtocol mSSLProto;

    QTcpSocket *mSocket;
    ConnectionState mState;
    QByteArray mData;

    QByteArray mServerGreeting;

    Job *mRunningJob;
    QQueue<Job*> mJobQueue;

    QTimer *mSocketTimer;
    int mSocketTimeout;

    QFile *mLogger;
};

}


#endif // KPOP3_SESSION_H
