/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef KPOP3_LISTJOB_H
#define KPOP3_LISTJOB_H

#include "job.h"
#include "message.h"

#include "kpop3_export.h"

namespace KPOP3 {

class KPOP3_EXPORT ListJob : public Job
{
    Q_OBJECT
public:
    virtual quint32 messageCount() const = 0;
    virtual quint64 totalSize() const = 0;
    virtual MessageList messages() const = 0;

protected:
    ListJob(Session *session);
};

}

#endif
