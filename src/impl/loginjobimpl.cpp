/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "loginjobimpl_p.h"
#include "job_p.h"
#include "kpop3_debug.h"

#include <QRegularExpression>
#include <QCryptographicHash>

#include <QFile>
#include <QDir>
#include <QStandardPaths>

static const sasl_callback_t callbacks[] = {
    { SASL_CB_ECHOPROMPT, nullptr, nullptr },
    { SASL_CB_NOECHOPROMPT, nullptr, nullptr },
    { SASL_CB_GETREALM, nullptr, nullptr },
    { SASL_CB_USER, nullptr, nullptr },
    { SASL_CB_AUTHNAME, nullptr, nullptr },
    { SASL_CB_PASS, nullptr, nullptr },
    { SASL_CB_CANON_USER, nullptr, nullptr },
    { SASL_CB_LIST_END, nullptr, nullptr }
};

using namespace KPOP3;

LoginJobImpl::LoginJobImpl(const QString &username, const QString &password,
                AuthenticationMethod authMethod, Session *session)
    : Job(*new JobPrivate(session))
    , mUsername(username)
    , mPassword(password)
    , mAuthMethod(authMethod)
    , mAuthPhase(NoPhase)
    , mSaslConn(Q_NULLPTR)
    , mSaslInteract(Q_NULLPTR)
{}

LoginJobImpl::~LoginJobImpl()
{}

bool LoginJobImpl::supportsAPOP()
{
    Q_D(Job);
    const QString greeting = QString::fromUtf8(d->session()->serverGreeting());
    const QRegularExpression rx(QStringLiteral("<[A-Za-z0-9\\.\\-_]+@[A-Za-z0-9\\.\\-_]+>$"),
                                QRegularExpression::CaseInsensitiveOption);
    const auto match = rx.match(greeting, 0, QRegularExpression::NormalMatch);
    if (match.hasMatch()) {
        mApopTimestamp = match.captured();
        return true;
    }
    return false;
}

void LoginJobImpl::trySASLAuthentication(const QByteArray &authMethod)
{
    Q_D(Job);
    mAuthPhase = SASLPhase;

    if (!saslInit()) {
        setError(AuthMethodNotSupported);
        emitResult();
        return;
    }
    qCDebug(KPOP3_LOG) << "SASL INIT: OK";

    int result = sasl_client_new("pop", d->session()->hostname().toLatin1().constData(),
                                 Q_NULLPTR, Q_NULLPTR, callbacks, 0, &mSaslConn);
    if (result != SASL_OK) {
        qCWarning(KPOP3_LOG) << "sasl_client_new failed with:" << result;
        setError(AuthMethodFailed);
        setErrorText(QString::fromUtf8(sasl_errdetail(mSaslConn)));
        return;
    }


    uint outlen = 0;
    const char *out;
    const char *mechusing = Q_NULLPTR;
    do {
        result = sasl_client_start(mSaslConn, authMethod.toLower().constData(), &mSaslInteract,
                                   &out, &outlen, &mechusing);
        qCDebug(KPOP3_LOG) << "SASL CLIENT START:" << result;
        if (result == SASL_INTERACT) {
            if (!saslInteract()) {
                sasl_dispose(&mSaslConn);
                setError(AuthMethodFailed);   //TODO: check up the actual error
                emitResult();
                return;
            }
        }
    } while (result == SASL_INTERACT);

    if (result != SASL_CONTINUE && result != SASL_OK) {
        qCDebug(KPOP3_LOG) << "sasl_client_start failed with:" << result;
        setError(AuthMethodFailed);
        setErrorText(QString::fromUtf8(sasl_errdetail(mSaslConn)));
        sasl_dispose(&mSaslConn);
        return;
    }

    qCDebug(KPOP3_LOG) << "Preferred authentication method is " << mechusing << ".";

    QByteArray cmd = "AUTH " + QByteArray(mechusing);
    if (outlen > 0) {
        cmd += " " + QByteArray::fromRawData(out, outlen).toBase64();
    }
    qCDebug(KPOP3_LOG) << "SASL AUTH:" << cmd;
    d->session()->sendCommand(cmd);
}

bool LoginJobImpl::saslInit()
{
#ifdef Q_OS_WIN32  //krazy:exclude=cpp
    QByteArray libInstallPath;
    Q_FOREACH (const QString &path, qCoreApp->libraryPaths()) {
        if (QDir(path).exists(QStringLiteral("sasl2"))) {
            libInstallPath = QFile::encodeName(QDir::toNativeSeparators(path + "/sasl2"));
            break;
        }
    QByteArray configPath = QFile::encodeName(QDir::toNativeSeparators(
        QStandardPaths::locate(QStandardPaths::ConfigLocation, QStringLiteral("sasl2"))));
    if (sasl_set_path(SASL_PATH_TYPE_PLUGIN, libInstallPath.data()) != SASL_OK ||
            sasl_set_path(SASL_PATH_TYPE_CONFIG, configPath.data()) != SASL_OK) {
        fprintf(stderr, "SASL path initialization failed!\n");
        return false;
    }
#endif
    if (sasl_client_init(Q_NULLPTR) != SASL_OK) {
        qCritical(KPOP3_LOG) << "SASL library initialization failed!";
        return false;
    }
    return true;
}

bool LoginJobImpl::saslInteract()
{
    qCDebug(KPOP3_LOG) << "SASL INTERACT";
    auto interact = (sasl_interact_t *) mSaslInteract;
    while (interact->id != SASL_CB_LIST_END) {
        qCDebug(KPOP3_LOG) << "SASL_INTERACT id: " << interact->id;
        switch (interact->id) {
        case SASL_CB_USER:
        case SASL_CB_AUTHNAME:
            qCDebug(KPOP3_LOG) << "SASL_CB_[USER|AUTHNAME]: " << mUsername;
            interact->result = strdup(mUsername.toUtf8().constData());
            interact->len = strlen((const char *) interact->result);
            break;
        case SASL_CB_PASS:
            qCDebug(KPOP3_LOG) << "SASL_CB_PASS: [hidden] ";
            interact->result = strdup(mPassword.toUtf8().constData());
            interact->len = strlen((const char *) interact->result);
            break;
        default:
            interact->result = 0;
            interact->len = 0;
            break;
        }
        interact++;
    }
    return true;
}

bool LoginJobImpl::saslAnswerChallenge(const QByteArray &response)
{
    Q_D(Job);

    const char *out;
    unsigned int outlen;
    int result;
    do {
        result = sasl_client_step(mSaslConn, response.isEmpty() ? 0 : response.data(),
                                       response.size(), &mSaslInteract, &out, &outlen);

        if (result == SASL_INTERACT) {
            if (!saslInteract()) {
                sasl_dispose(&mSaslConn);
                return false;
            }
        }
    } while (result == SASL_INTERACT);
    if (result != SASL_CONTINUE && result != SASL_OK) {
        qCDebug(KPOP3_LOG) << "sasl_client_step failed with: " << result;
        sasl_dispose(&mSaslConn);
        return false;
    }

    QByteArray cmd = QByteArray::fromRawData(out, outlen).toBase64();
    d->session()->sendCommand(cmd);
    return true;
}


void LoginJobImpl::authenticateWithAPOP()
{
    Q_D(Job);

    QByteArray cmd = "APOP " + mUsername.toLatin1() + " ";

    QCryptographicHash hash(QCryptographicHash::Md5);
    qDebug() << mApopTimestamp << mPassword;
    hash.addData(mApopTimestamp.toLatin1());
    hash.addData(mPassword.toLatin1());
    cmd += hash.result().toHex();

    mAuthPhase = APOPPhase;
    d->session()->sendCommand(cmd);
}

void LoginJobImpl::authenticateWithUser()
{
    Q_D(Job);
    mAuthPhase = USERPhase;
    d->session()->sendCommand("USER " + mUsername.toLatin1());
}

void LoginJobImpl::authenticateWithPass()
{
    Q_D(Job);
    mAuthPhase = PASSPhase;
    d->session()->sendCommand("PASS " + mPassword.toLatin1());
}

void LoginJobImpl::doStart()
{
    switch (mAuthMethod) {
    case PLAIN:
        trySASLAuthentication("PLAIN");
        break;
    case LOGIN:
        trySASLAuthentication("LOGIN");
        break;
    case CRAM_MD5:
        trySASLAuthentication("CRAM-MD5");
        break;
    case DIGEST_MD5:
        trySASLAuthentication("DIGEST-MD5");
        break;
    case NTLM:
        trySASLAuthentication("NTLM");
        break;
    case GSSAPI:
        trySASLAuthentication("GSSAPI");
        break;
    case CLEAR:
        authenticateWithUser();
        break;
    case APOP:
        if (supportsAPOP()) {
            authenticateWithAPOP();
        } else {
            setError(AuthMethodNotSupported);
            emitResult();
        }
        break;
    }
}

void LoginJobImpl::handleResponse(const QByteArray &response) 
{
    Q_D(Job);
    if (mAuthPhase == APOPPhase) {
        // success
        d->session()->setState(TransactionState);
    } else if (mAuthPhase == SASLPhase) {
        if (response.startsWith("+OK")) {
            d->session()->setState(TransactionState);
        } else if (response.startsWith("+")) {
            if (!saslAnswerChallenge(QByteArray::fromBase64(response.mid(2)))) {
                emitResult(); //error :(
            }
            return;
        } else {
            setError(InvalidServerResponse);
        }
    } else if (mAuthPhase == USERPhase) {
        if (response.startsWith("+OK")) {
            authenticateWithPass();
            return;
        } else {
            setError(InvalidServerResponse);
        }
    } else if (mAuthPhase == PASSPhase) {
        // Done, success!
        d->session()->setState(TransactionState);
    }

    emitResult();
}

Job::HandlerResponse LoginJobImpl::handleErrorReplies(const QByteArray &response)
{
    // We tried APOP and got rejected -> auth error
    // We tried USER and got rejected -> auth error
    if (mAuthPhase == APOPPhase ||
        mAuthPhase == USERPhase ||
        mAuthPhase == PASSPhase) {
        setError(BadCredentials);
        setErrorText(QString::fromUtf8(response.mid(5)));
        emitResult();
        return Handled;
    }

    // We tried SASL and got rejected
    if (mAuthPhase == SASLPhase) {
        setError(AuthMethodNotSupported);
        emitResult();
        return Handled;
    }

    return NotHandled;
}
