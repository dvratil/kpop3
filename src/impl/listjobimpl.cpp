/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "listjobimpl_p.h"
#include "job_p.h"
#include "message.h"

using namespace KPOP3;

ListJobImpl::ListJobImpl(ListingMode mode, Session *session)
    : ListJob(session)
    , mMsg(0)
    , mTotalSize(0)
    , mMode(mode)
{}

ListJobImpl::ListJobImpl(MsgID msg, ListingMode mode, Session *session)
    : ListJob(session)
    , mMsg(msg)
    , mTotalSize(0)
    , mMode(mode)
{}

ListJobImpl::~ListJobImpl()
{}

quint32 ListJobImpl::messageCount() const
{
    return mMessages.count();
}

quint64 ListJobImpl::totalSize() const
{
    return mTotalSize;
}

MessageList ListJobImpl::messages() const
{
    return mMessages;
}

void ListJobImpl::doStart()
{
    Q_D(Job);

    QByteArray cmd;
    if (mMode == LIST) {
        cmd = "LIST";
    } else {
        cmd = "UIDL";
    }
    if (mMsg > 0) {
        cmd += " " + QByteArray::number(mMsg);
    }

    d->session()->sendCommand(cmd);
}

void ListJobImpl::handleResponse(const QByteArray &response)
{
    Q_D(Job);

    // End of listing, there will be no more responses
    if (response == ".") {
        emitResult();
        return;
    }

    const auto parts = response.split(' ');
    if (mMsg > 0) {
        if (parts.size() >= 3 && parts[0] == "+OK") {
            Message msg = (mMode == LIST)
                            ? Message(parts[1].toUInt(), parts[2].toULongLong())
                            : Message(parts[1].toUInt(), parts[2]);
            mMessages.push_back(std::move(msg));
            mTotalSize += msg.size;
            emitResult();
            return;
        }
    } else {
        if (parts.size() > 0 && parts[0] == "+OK") {
            // OK, we'll receive listing responses next
        } else if (parts.size() > 1) {
            Message msg = (mMode == LIST)
                            ? Message(parts[0].toUInt(), parts[1].toULongLong())
                            : Message(parts[0].toUInt(), parts[1]);
            mTotalSize += msg.size;
            mMessages.push_back(std::move(msg));
        }
        return;
    }

    setError(InvalidServerResponse);
    emitResult();
}
