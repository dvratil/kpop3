/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "statjobimpl_p.h"
#include "job_p.h"

using namespace KPOP3;

StatJobImpl::StatJobImpl(Session *session)
    : StatJob(session)
    , mMsgCount(0)
    , mSize(0)
{}

StatJobImpl::~StatJobImpl()
{}

quint32 StatJobImpl::messageCount() const
{
    return mMsgCount;
}

quint64 StatJobImpl::mailboxSize() const
{
    return mSize;
}

void StatJobImpl::doStart()
{
    Q_D(Job);
    d->session()->sendCommand("STAT");
}

void StatJobImpl::handleResponse(const QByteArray &response)
{
    const auto data = response.split(' ');
    if (data.size() < 3 || data[0] != "+OK") {
        setError(Job::InvalidServerResponse);
        emitResult();
        return;
    }

    bool ok = false;
    mMsgCount = data[1].toUInt(&ok);
    if (!ok) {
        setError(Job::InvalidServerResponse);
        emitResult();
        return;
    }

    mSize = data[2].toULongLong(&ok);
    if (!ok) {
        setError(Job::InvalidServerResponse);
        emitResult();
        return;
    }

    emitResult();
}
