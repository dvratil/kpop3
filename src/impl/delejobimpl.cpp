/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "delejobimpl_p.h"
#include "job_p.h"
#include "message.h"

using namespace KPOP3;

DeleJobImpl::DeleJobImpl(MsgID msg, Session *session)
    : Job(*new JobPrivate(session))
    , mMsg(msg)
{}

DeleJobImpl::~DeleJobImpl()
{}

void DeleJobImpl::doStart()
{
    Q_D(Job);
    d->session()->sendCommand("DELE " + QByteArray::number(mMsg));
}

void DeleJobImpl::handleResponse(const QByteArray &response)
{
    if (!response.startsWith("+OK")) {
        setError(InvalidServerResponse);
    }

    emitResult();
}
