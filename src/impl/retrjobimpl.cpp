/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "retrjobimpl_p.h"
#include "job_p.h"

using namespace KPOP3;

RetrJobImpl::RetrJobImpl(MsgID msg, Session *session)
    : RetrJob(session)
    , mMsgID(msg)
    , mLinesCount(-1)
{}

RetrJobImpl::RetrJobImpl(MsgID msg, int linesCount, Session *session)
    : RetrJob(session)
    , mMsgID(msg)
    , mLinesCount(linesCount)
{}

RetrJobImpl::~RetrJobImpl()
{}

Message RetrJobImpl::message() const
{
    return mMessage;
}

void RetrJobImpl::doStart()
{
    Q_D(Job);
    if (mLinesCount > -1) {
        d->session()->sendCommand("TOP "
                                  + QByteArray::number(mMsgID)
                                  + " "
                                  + QByteArray::number(mLinesCount));
    } else {
        d->session()->sendCommand("RETR " + QByteArray::number(mMsgID));
    }
}

void RetrJobImpl::handleResponse(const QByteArray &response)
{
    if (response.startsWith("+OK")) {
        // We will start receiving now
        return;
    } else if (response == ".") {
        // Line that only has one dot means end of data
        mMessage.size = mMessage.data.size();
        emitResult();
    } else {
        if (!mMessage.data.isEmpty()) {
            mMessage.data += "\r\n";
        }
        // Line starting with two dots means the data line starts with a dot
        if (response.startsWith("..")) {
            mMessage.data += QByteArray::fromRawData(response.constData() + 1, response.size() - 1);
        } else {
            mMessage.data += response;
        }
    }
}
