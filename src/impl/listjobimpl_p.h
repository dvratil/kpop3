/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef KPOP3_LISTJOBIMPL_P_H
#define KPOP3_LISTJOBIMPL_P_H

#include "listjob.h"
#include "message.h"

namespace KPOP3 {

class ListJobImpl : public ListJob
{
    Q_OBJECT

public:
    enum ListingMode {
        LIST,
        UIDL
    };

    ListJobImpl(ListingMode mode, Session *session);
    ListJobImpl(MsgID msg, ListingMode mode, Session *session);
    ~ListJobImpl();

    quint32 messageCount() const Q_DECL_OVERRIDE;
    quint64 totalSize() const Q_DECL_OVERRIDE;
    MessageList messages() const Q_DECL_OVERRIDE;

protected:
    void doStart() Q_DECL_OVERRIDE;
    void handleResponse(const QByteArray &response) Q_DECL_OVERRIDE;

private:
    MsgID mMsg;
    quint64 mTotalSize;
    QVector<Message> mMessages;
    ListingMode mMode;
};

} // namespace KPOP3

#endif
