/*
 * Copyright (C) 2015 Daniel Vrátil <dvratil@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "session_p.h"
#include "job.h"

#include "kpop3_debug.h"

#include <QCoreApplication>
#include <QString>
#include <QTimer>
#include <QFile>
#include <QTcpSocket>
#include <QSslSocket>

using namespace KPOP3;

Session::Session(const QString &hostname, quint16 port,
                 bool useSSL, QSsl::SslProtocol sslProto,
                 QObject *parent)
    : QObject(parent)
    , mProxy(QNetworkProxy::NoProxy)
    , mHost(hostname)
    , mPort(port)
    , mUseSSL(useSSL)
    , mSSLProto(sslProto)
    , mSocket(Q_NULLPTR)
    , mState(DisconnectedState)
    , mRunningJob(Q_NULLPTR)
    , mSocketTimer(new QTimer(this))
    , mSocketTimeout(30000) // 30 seconds
    , mLogger(Q_NULLPTR)
{
    if (qEnvironmentVariableIsSet("KPOP3_SESSION_LOGFILE")) {
        static int instance = 0;
        ++instance;
        const QString file = QString::fromLatin1("%1.%2.%3")
                .arg(QString::fromUtf8(qgetenv("KPOP3_SESSION_LOGFILE")))
                .arg(qApp->applicationPid())
                .arg(instance);
        mLogger = new QFile(file);
        if (!mLogger->open(QIODevice::WriteOnly | QIODevice::Truncate)) {
            qCWarning(KPOP3_LOG) << "Failed to open session log file" << file << ":" << mLogger->errorString();
        }
    }

    if (mUseSSL) {
        auto socket = new QSslSocket(this);
        socket->setProtocol(mSSLProto);
        mSocket = socket;
        connect(socket, &QSslSocket::encrypted, this, &Session::onSocketEncrypted);
    } else {
        mSocket = new QTcpSocket(this);
    }
    mSocket->setProxy(mProxy);

    connect(mSocket, &QAbstractSocket::stateChanged, this, &Session::onSocketStateChanged);
    connect(mSocket, &QIODevice::readyRead, this, &Session::onReadyRead);
    connect(mSocket, static_cast<void(QTcpSocket::*)(QAbstractSocket::SocketError)>(&QTcpSocket::error),
            this, &Session::onSocketError);

    connect(mSocketTimer, &QTimer::timeout, this, &Session::onSocketTimeout);

    reconnect();
}

Session::~Session()
{
    if (mLogger) {
        mLogger->close();
        delete mLogger;
    }

    mSocket->close();
    delete mSocket;
}

void Session::setProxy(const QNetworkProxy &proxy)
{
    mProxy = proxy;
    mSocket->setProxy(mProxy);

    reconnect();
}

void Session::setState(ConnectionState state)
{
    if (mState != state) {
        mState = state;
        Q_EMIT stateChanged(mState);
    }
}

void Session::addJob(Job *job)
{
    mJobQueue.enqueue(job);
    connect(job, &QObject::destroyed, this, &Session::onJobDestroyed);
    startNext();
}

void Session::startNext()
{
    if (mRunningJob || mJobQueue.isEmpty() || mServerGreeting.isEmpty()) {
        return;
    }

    restartSocketTimer();
    mRunningJob = mJobQueue.dequeue();
    connect(mRunningJob, &KJob::finished, this, &Session::onJobFinished);
    mRunningJob->doStart();
}

void Session::onJobFinished(KJob *)
{
    mRunningJob = Q_NULLPTR;
    stopSocketTimer();
    startNext();
}

void Session::onJobDestroyed(QObject *job)
{
    mJobQueue.removeOne(static_cast<Job*>(job));
}

void Session::reconnect()
{
    if (mSocket->state() == QAbstractSocket::ConnectedState) {
        close();
    }

    if (QSslSocket *ssl = qobject_cast<QSslSocket*>(mSocket)) {
        ssl->connectToHostEncrypted(mHost, mPort, QIODevice::ReadWrite);
    } else {
        mSocket->connectToHost(mHost, mPort, QIODevice::ReadWrite);
    }
}

void Session::close()
{
    mSocket->disconnectFromHost();
}

void Session::onSocketEncrypted()
{
    setState(AuthorizationState);
    startNext();
}

void Session::onSocketStateChanged(QAbstractSocket::SocketState state)
{
    if (state == QAbstractSocket::ConnectedState) {
        if (!qobject_cast<QSslSocket*>(mSocket)) {
            setState(AuthorizationState);
            startNext();
        }
    } else if (state == QAbstractSocket::UnconnectedState) {
        setState(DisconnectedState);
        if (mRunningJob) {
            mRunningJob->connectionLost();
        }
    }
}

void Session::onSocketError()
{
    qCWarning(KPOP3_LOG) << "Socket error:" << mSocket->errorString();
    if (QSslSocket *ssl = qobject_cast<QSslSocket*>(mSocket)) {
        Q_FOREACH (const auto &sslError, ssl->sslErrors()) {
            qCWarning(KPOP3_LOG) << sslError.errorString();
        }
    }
}

void Session::onSocketTimeout()
{
    mSocket->close();
}

void Session::onReadyRead()
{
    stopSocketTimer();

    while (!mData.isEmpty() || mSocket->bytesAvailable()) {
        // RFC1939: Responses may be up to 512 characters long, including the
        // terminating CRLF
        mData += mSocket->read(512);
        const int idx = mData.indexOf("\r\n");
        if (idx == -1) {
            break;
        }
        const QByteArray data = QByteArray::fromRawData(mData.constData(), idx);
        if (mLogger) {
            mLogger->write("S: " + data + "\n");
            mLogger->flush();
        }
        if (mState == DisconnectedState) {
            stopSocketTimer();
        } else if (mState == AuthorizationState && !mRunningJob && mServerGreeting.isEmpty()) {
            mServerGreeting = data;
            startNext();
        } else if (mRunningJob) {
            if (mRunningJob) {
                restartSocketTimer();
                if (mData.startsWith("-ERR")) {
                    if (mRunningJob->handleErrorReplies(data) == Job::NotHandled) {
                        mRunningJob->handleResponse(data);
                    }
                } else {
                    mRunningJob->handleResponse(data);
                }
            }
        }

        mData.remove(0, idx + 2);
    }
}


void Session::sendCommand(const QByteArray &command)
{
    restartSocketTimer();

    if (mLogger) {
        mLogger->write("C: " + command + "\n");
        mLogger->flush();
    }

    mSocket->write(command + "\r\n");
}

void Session::setSocketTimeout(int timeout)
{
    const bool isActive = mSocketTimer->isActive();

    if (isActive) {
        mSocketTimer->stop();
    }

    mSocketTimeout = timeout;
    if (isActive) {
        mSocketTimer->start(mSocketTimeout);
    }
}

void Session::startSocketTimer()
{
    if (mSocketTimeout < 0) {
        return;
    }

    if (!mSocketTimer->isActive()) {
        mSocketTimer->start(mSocketTimeout);
    }
}

void Session::stopSocketTimer()
{
    mSocketTimer->stop();
}

void Session::restartSocketTimer()
{
    stopSocketTimer();
    startSocketTimer();
}
