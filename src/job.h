/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef KPOP3_JOB_H
#define KPOP3_JOB_H

#include "kpop3_export.h"

#include <KJob>

namespace KPOP3 {

class Session;
class SessionPrivate;
class JobPrivate;
class KPOP3_EXPORT Job : public KJob
{
    Q_OBJECT

public:
    enum Error {
        InvalidServerResponse = UserDefinedError,
        UnsupportedCommand,
        AuthMethodNotSupported,
        AuthMethodFailed,
        BadCredentials
    };

    virtual ~Job();

    Session *session() const;
    void start() Q_DECL_OVERRIDE;

protected:
    virtual void doStart() = 0;
    virtual void handleResponse(const QByteArray &response);
    virtual void connectionLost();

protected:
    enum HandlerResponse {
        Handled = 0,
        NotHandled
    };

    virtual HandlerResponse handleErrorReplies(const QByteArray &response);

    explicit Job(Session *session);
    explicit Job(JobPrivate &dd);

    JobPrivate *const d_ptr;
    friend class Session;
    Q_DECLARE_PRIVATE(Job)
};

}

#endif
