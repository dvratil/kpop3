/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef KPOP3_STATJOB_H
#define KPOP3_STATJOB_H

#include "job.h"
#include "kpop3_export.h"

namespace KPOP3 {

class KPOP3_EXPORT StatJob : public Job
{
    Q_OBJECT
public:
    /**
     * Returns the amount of messages in the mailbox.
     */
    virtual quint32 messageCount() const = 0;

    /**
     * Returns the total size of all messages in the mailbox in bytes.
     */
    virtual quint64 mailboxSize() const = 0;

protected:
    StatJob(Session *session);
};

}

#endif
