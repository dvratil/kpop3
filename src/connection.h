/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef KPOP3_CONNECTION_H
#define KPOP3_CONNECTION_H


#include <QObject>
#include <QSsl>

#include "kpop3_export.h"

#include "enums.h"
#include "message.h"

class QNetworkProxy;

namespace KPOP3
{

class ListJob;
class RetrJob;
class Job;
class StatJob;
class CapaJob;
class ConnectionPrivate;

class KPOP3_EXPORT Connection : public QObject
{
    Q_OBJECT
public:
    Connection(const QString &host, quint16 port, QObject *parent = Q_NULLPTR);
    Connection(const QString &host, quint16 port, QSsl::SslProtocol sslProtocol,
               QObject *parent = Q_NULLPTR);
    ~Connection();

    ConnectionState state() const;
    void setProxy(const QNetworkProxy &proxy);
    QNetworkProxy proxy() const;

    CapaJob *capa();

    ListJob *list();
    ListJob *list(MsgID msg);

    ListJob *uidl();
    ListJob *uidl(MsgID msg);

    RetrJob *retr(MsgID msg);
    RetrJob *top(MsgID msg, int lines);

    Job *dele(MsgID msg);
    Job *rset();

    StatJob *stat();

    Job *login(const QString &username, const QString &password,
               AuthenticationMethod method);

    Job *quit();

Q_SIGNALS:
    void stateChanged(ConnectionState newState);

private:
    ConnectionPrivate * const d_ptr;
    Q_DECLARE_PRIVATE(Connection)
    Q_DISABLE_COPY(Connection)
};

}

#endif
