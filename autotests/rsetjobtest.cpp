/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include <QObject>
#include <QTest>
#include <QSignalSpy>

#include "fakeserver.h"
#include "connection.h"
#include "job.h"

using namespace KPOP3;

class RsetJobTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testRset_data()
    {
        QTest::addColumn<QList<QByteArray>>("scenario");
        QTest::addColumn<bool>("success");

        QList<QByteArray> scenario;
        scenario << FakeServer::greeting()
                 << "C: RSET"
                 << "S: +OK maildrop has 10 messages";
        QTest::newRow("simple RSET, success") << scenario << true;

        // RFC does not mention -ERR as possible response to RSET, but better
        // make sure we can handle it
        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: RSET"
                 << "S: -ERR something went wrong";
        QTest::newRow("simple RSET, error") << scenario << false;
    }

    void testRset()
    {
        QFETCH(QList<QByteArray>, scenario);
        QFETCH(bool, success);

        FakeServer server;
        server.setScenario(scenario);
        server.startAndWait();

        Connection conn(QStringLiteral("127.0.0.1"), 5989);
        auto job = conn.rset();
        QSignalSpy spy(job, &KJob::result);
        QVERIFY(spy.wait());

        QCOMPARE(!job->error(), success);

        server.quit();
    }
};

QTEST_GUILESS_MAIN(RsetJobTest)

#include "rsetjobtest.moc"



