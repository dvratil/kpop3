/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include <QObject>
#include <QTest>
#include <QSignalSpy>

#include "fakeserver.h"
#include "connection.h"
#include "job.h"

using namespace KPOP3;

class QuitJobTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testQuit_data()
    {
        QTest::addColumn<QList<QByteArray>>("scenario");
        QTest::addColumn<bool>("success");

        QList<QByteArray> scenario;
        scenario << FakeServer::greeting()
                 << "C: QUIT"
                 << "S: +OK buh-bye"
                 << "X";
        QTest::newRow("server replies and quits, should succeed") << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: QUIT"
                 << "X";
        QTest::newRow("server quits, should succeed") << scenario << true;

        // -ERR should never happen according to RFC, but better make sure we
        // don't get stuck
        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: QUIT"
                 << "S: -ERR [QUIT] Please don't go!";
        QTest::newRow("should fail") << scenario << false;

    }

    void testQuit()
    {
        QFETCH(QList<QByteArray>, scenario);
        QFETCH(bool, success);

        FakeServer server;
        server.setScenario(scenario);
        server.startAndWait();

        Connection conn(QStringLiteral("127.0.0.1"), 5989);
        auto job = conn.quit();
        QSignalSpy spy(job, &KJob::result);
        QVERIFY(spy.wait());

        QCOMPARE(!job->error(), success);

        server.quit();
    }
};

QTEST_GUILESS_MAIN(QuitJobTest)

#include "quitjobtest.moc"
