/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include <QObject>
#include <QTest>
#include <QSignalSpy>

#include "fakeserver.h"
#include "connection.h"
#include "job.h"

Q_DECLARE_METATYPE(KPOP3::AuthenticationMethod)

using namespace KPOP3;

class LoginJobTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testLogin_data()
    {
        QTest::addColumn<KPOP3::AuthenticationMethod>("method");
        QTest::addColumn<QString>("username");
        QTest::addColumn<QString>("password");
        QTest::addColumn<QList<QByteArray>>("scenario");
        QTest::addColumn<bool>("success");

        QList<QByteArray> scenario;
        scenario << FakeServer::greeting()
                 << "C: APOP user1@domain.com 839d6483d6a58ee0107c8ca1ed3f4e66"
                 << "S: +OK APOP authentication successful";
        QTest::newRow("APOP supported, should succeed") << KPOP3::APOP
                << QStringLiteral("user1@domain.com") << QStringLiteral("password")
                << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: APOP user1@domain.com 16cf4ea708d4c3757c5cb1b6bd5b3d5b"
                 << "S: -ERR [AUTH] authenticating: authentication failure";
        QTest::newRow("APOP supported, should fail") << KPOP3::APOP
                << QStringLiteral("user1@domain.com") << QStringLiteral("badpassword")
                << scenario << false;

        scenario.clear();
        scenario << FakeServer::greeting(false);
        QTest::newRow("APOP unsupported, should fail") << KPOP3::APOP
                << QStringLiteral("user1@domain.com") << QStringLiteral("password")
                << scenario << false;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: USER user1@domain.com"
                 << "S: +OK Name is a valid mailbox"
                 << "C: PASS password"
                 << "S: +OK Mailbox locked and ready";
        QTest::newRow("CLEAR auth, should succeed") << KPOP3::CLEAR
                << QStringLiteral("user1@domain.com") << QStringLiteral("password")
                << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: USER baduser@domain.com"
                 << "S: -ERR [AUTH] No such mailbox";
        QTest::newRow("CLEAR auth, bad mailbox, should fail") << KPOP3::CLEAR
                << QStringLiteral("baduser@domain.com") << QStringLiteral("password")
                << scenario << false;

        scenario.clear();
        scenario << FakeServer::greeting()
                << "C: USER user1@domain.com"
                << "S: +OK Name is a valid mailbox"
                << "C: PASS badpassword"
                << "S: -ERR [AUTH] Invalid login";
        QTest::newRow("CLEAR auth, bad pass, should fail") << KPOP3::CLEAR
                << QStringLiteral("user1@domain.com") << QStringLiteral("badpassword")
                << scenario << false;
    }

    void testLogin()
    {
        QFETCH(KPOP3::AuthenticationMethod, method);
        QFETCH(QString, username);
        QFETCH(QString, password);
        QFETCH(QList<QByteArray>, scenario);
        QFETCH(bool, success);

        FakeServer server;
        server.setScenario(scenario);
        server.startAndWait();

        Connection conn(QStringLiteral("127.0.0.1"), 5989);
        auto job = conn.login(username, password, method);
        QSignalSpy spy(job, &KJob::result);
        QVERIFY(spy.wait());

        QCOMPARE(!job->error(), success);

        server.quit();
    }
};

QTEST_GUILESS_MAIN(LoginJobTest)

#include "loginjobtest.moc"
