/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include <QObject>
#include <QTest>
#include <QSignalSpy>

#include "job.h"
#include "fakeserver.h"
#include "connection.h"

using namespace KPOP3;

class DeleJobTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testDele_data()
    {
        QTest::addColumn<quint32>("msgId");
        QTest::addColumn<QList<QByteArray>>("scenario");
        QTest::addColumn<bool>("success");

        QList<QByteArray> scenario;
        scenario << FakeServer::greeting()
                 << "C: DELE 10"
                 << "S: +OK message 10 deleted";
        QTest::newRow("simple delete, success") << 10u << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: DELE 34"
                 << "S: -ERR message 34 already deleted";
        QTest::newRow("simple delete, error") << 34u << scenario << false;
    }

    void testDele()
    {
        QFETCH(quint32, msgId);
        QFETCH(QList<QByteArray>, scenario);
        QFETCH(bool, success);

        FakeServer server;
        server.setScenario(scenario);
        server.startAndWait();

        Connection conn(QStringLiteral("127.0.0.1"), 5989);

        auto job = conn.dele(msgId);
        QSignalSpy spy(job, &KJob::result);
        QVERIFY(spy.wait());

        QCOMPARE(!job->error(), success);

        server.quit();
    }
};

QTEST_GUILESS_MAIN(DeleJobTest)

#include "delejobtest.moc"


