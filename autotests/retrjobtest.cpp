/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include <QObject>
#include <QTest>
#include <QSignalSpy>

#include "fakeserver.h"
#include "connection.h"
#include "retrjob.h"

using namespace KPOP3;

class RetrJobTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testRetr_data()
    {
        QTest::addColumn<quint32>("msgId");
        QTest::addColumn<int>("topLines");
        QTest::addColumn<QByteArray>("msg");
        QTest::addColumn<QList<QByteArray>>("scenario");
        QTest::addColumn<bool>("success");

        QList<QByteArray> scenario;
        QByteArray msg = "From: test@example.com\r\nTo: you@example.com\r\nSubject: Hello\r\n\r\n"
                         "Hey, how are you?\r\nDan\r\n";
        scenario << FakeServer::greeting()
                 << "C: RETR 1"
                 << "S: +OK message follows"
                 << "S: " + msg
                 << "S: .";
        QTest::newRow("RETR simple message") << 1u << -1 << msg << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: RETR 2"
                 << "S: +OK message follows"
                 << "S: From: test@example.com\r\nTo: you@example.com\r\nSubject: Help\r\n\r\n"
                    "....hey, are you there?\r\n...\r\n"
                 << "S: .";
        QTest::newRow("RETR line starting with dots") << 2u << -1
                << QByteArray("From: test@example.com\r\nTo: you@example.com\r\nSubject: Help\r\n\r\n"
                              "...hey, are you there?\r\n..\r\n")
                << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: RETR 3"
                 << "S: -ERR No such message";
        QTest::newRow("RETR no such message") << 3u << -1 << QByteArray() << scenario << false;

        msg = "From test@example.com\r\nTo: you@example.com\r\nSubject: Hello\r\n\r\n"
              "Line 1\r\nLine 2\r\nLine 3";
        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: TOP 4 3"
                 << "S: " + msg
                << "S: .";
        QTest::newRow("TOP simple message") << 4u << 3 << msg << scenario << true;
    }

    void testRetr()
    {
        QFETCH(quint32, msgId);
        QFETCH(int, topLines);
        QFETCH(QByteArray, msg);
        QFETCH(QList<QByteArray>, scenario);
        QFETCH(bool, success);

        FakeServer server;
        server.setScenario(scenario);
        server.startAndWait();

        Connection conn(QStringLiteral("127.0.0.1"), 5989);
        RetrJob *retr = (topLines == -1) ? conn.retr(msgId) : conn.top(msgId, topLines);
        QSignalSpy spy(retr, &KJob::result);
        QVERIFY(spy.wait());

        QCOMPARE(!retr->error(), success);
        QCOMPARE(retr->message().size, (quint32)msg.size());
        QCOMPARE(retr->message().data, msg);
        QCOMPARE(retr->message().data.size(), msg.size());

        server.quit();
    }
};

QTEST_GUILESS_MAIN(RetrJobTest)

#include "retrjobtest.moc"


