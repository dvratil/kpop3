/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include <QObject>
#include <QTest>
#include <QSignalSpy>

#include "fakeserver.h"
#include "connection.h"
#include "statjob.h"

using namespace KPOP3;

class StatJobTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testStat_data()
    {
        QTest::addColumn<QList<QByteArray>>("scenario");
        QTest::addColumn<quint32>("count");
        QTest::addColumn<quint64>("size");
        QTest::addColumn<bool>("success");

        QList<QByteArray> scenario;
        scenario << FakeServer::greeting()
                 << "C: STAT"
                 << "S: +OK 5972 578899082";
        QTest::newRow("random result, should succeed") << scenario << 5972u << 578899082ull << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: STAT"
                 << "S: +OK 0 0";
        QTest::newRow("empty mailbox, should succeed") << scenario << 0u << 0ull << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: STAT"
                 << "S: +OK 42 8432341234 arbitrary data";
        QTest::newRow("random result with additional data, should succeed")
                << scenario << 42u << 8432341234ull << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: STAT"
                 << "S: +OK 6332 invalid data";
        QTest::newRow("invalid success response, should fail") << scenario << 6332u << 0ull << false;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: STAT"
                 << "S: -ERR Something went wrong";
        QTest::newRow("error response, should fail") << scenario << 0u << 0ull << false;
    }

    void testStat()
    {
        QFETCH(QList<QByteArray>, scenario);
        QFETCH(quint32, count);
        QFETCH(quint64, size);
        QFETCH(bool, success);

        FakeServer server;
        server.setScenario(scenario);
        server.startAndWait();

        Connection conn(QStringLiteral("127.0.0.1"), 5989);
        StatJob *stat = conn.stat();
        QSignalSpy spy(stat, &KJob::result);
        QVERIFY(spy.wait());

        QCOMPARE(!stat->error(), success);
        QCOMPARE(stat->messageCount(), count);
        QCOMPARE(stat->mailboxSize(), size);

        server.quit();
    }
};

QTEST_GUILESS_MAIN(StatJobTest)

#include "statjobtest.moc"

