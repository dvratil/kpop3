/*
    Copyright (c) 2016 Daniel Vrátil <dvratil@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include <QObject>
#include <QTest>
#include <QSignalSpy>

#include "fakeserver.h"
#include "connection.h"
#include "listjob.h"

using namespace KPOP3;

Q_DECLARE_METATYPE(KPOP3::MessageList)

class ListJobTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testList_data()
    {
        QTest::addColumn<quint32>("msgId");
        QTest::addColumn<bool>("uidl");
        QTest::addColumn<quint64>("expSize");
        QTest::addColumn<MessageList>("expMsgs");
        QTest::addColumn<QList<QByteArray>>("scenario");
        QTest::addColumn<bool>("success");

        QList<QByteArray> scenario;
        scenario << FakeServer::greeting()
                 << "C: LIST"
                 << "S: +OK scan listing follows"
                 << "S: 3 91099"
                 << "S: 5 83340"
                 << "S: 15 90765"
                 << "S: .";
        QTest::newRow("LIST simple listing, 3 responses") << 0u << false << 265204ull
                << MessageList{ { 3, 91099 }, { 5, 83340 }, { 15, 90765 } }
                << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: LIST"
                 << "S: +OK scan listing follows"
                 << "S: .";
        QTest::newRow("LIST simple listing, 0 responses") << 0u << false << 0ull
                << MessageList() << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: LIST 16"
                 << "S: +OK 16 358241";
        QTest::newRow("LIST message listing, success") << 16u << false << 358241ull
                << MessageList{ { 16, 358241 } } << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: LIST 6932"
                 << "S: -ERR No such message";
        QTest::newRow("LIST message listing, error") << 6932u << false << 0ull
                << MessageList() << scenario << false;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: UIDL"
                 << "S: +OK unique-id listing follows"
                 << "S: 3 whqtswO00WBw418f9t5JxYwZ"
                 << "S: 4 QhdPYR:00WBw1Ph7x7"
                 << "S: .";
        QTest::newRow("UIDL simple listing, 2 responses") << 0u << true << 0ull
                << MessageList{ { 3, "whqtswO00WBw418f9t5JxYwZ" },
                                { 4, "QhdPYR:00WBw1Ph7x7" } }
                << scenario << true;

        scenario.clear();
        scenario << FakeServer::greeting()
                 << "C: UIDL 4"
                 << "S: +OK 4 QhdPYR:00WBw1Ph7x7";
        QTest::newRow("UIDL message listing, success") << 4u << true << 0ull
                << MessageList{ { 4, "QhdPYR:00WBw1Ph7x7" } } << scenario << true;
    }

    void testList()
    {
        QFETCH(quint32, msgId);
        QFETCH(bool, uidl);
        QFETCH(quint64, expSize);
        QFETCH(MessageList, expMsgs);
        QFETCH(QList<QByteArray>, scenario);
        QFETCH(bool, success);

        FakeServer server;
        server.setScenario(scenario);
        server.startAndWait();

        Connection conn(QStringLiteral("127.0.0.1"), 5989);

        ListJob *list = uidl ? (msgId == 0) ? conn.uidl() : conn.uidl(msgId)
                             : (msgId == 0) ? conn.list() : conn.list(msgId);
        QSignalSpy spy(list, &KJob::result);
        QVERIFY(spy.wait());

        QCOMPARE(!list->error(), success);

        QCOMPARE(list->messageCount(), (quint32)expMsgs.count());
        QCOMPARE(list->totalSize(), expSize);

        const auto msgs = list->messages();
        QCOMPARE(msgs.count(), expMsgs.count());
        for (auto it = msgs.constBegin(), expIt = expMsgs.constBegin(); it != msgs.constEnd(); ++it, ++expIt) {
            QCOMPARE(it->id, expIt->id);
            QCOMPARE(it->size, expIt->size);
            QCOMPARE(it->uid, expIt->uid);
        }

        server.quit();
    }
};

QTEST_GUILESS_MAIN(ListJobTest)

#include "listjobtest.moc"

