/*
 * Copyright (C) 2015 Daniel Vrátil <dvratil@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QEventLoop>
#include <QScopedPointer>

#include "connection.h"
#include "job.h"

#include <iostream>

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    app.setApplicationName(QStringLiteral("kpoptest"));

    QCommandLineParser parser;
    parser.addHelpOption();
    const QCommandLineOption hostOpt({ QStringLiteral("H") }, QStringLiteral("Host"), QStringLiteral("host"));
    const QCommandLineOption portOpt({ QStringLiteral("p") }, QStringLiteral("Port"), QStringLiteral("port"));
    const QCommandLineOption userOpt({ QStringLiteral("u") }, QStringLiteral("Username"), QStringLiteral("user"));
    const QCommandLineOption passOpt({ QStringLiteral("P") }, QStringLiteral("Password"), QStringLiteral("password"));
    const QCommandLineOption sslOpt({ QStringLiteral("ssl") }, QStringLiteral("Use SSL"));
    parser.addOption(hostOpt);
    parser.addOption(portOpt);
    parser.addOption(userOpt);
    parser.addOption(passOpt);
    parser.addOption(sslOpt);
    parser.process(app);

    const QString host = parser.value(hostOpt);
    const quint16 port = parser.value(portOpt).toUInt();
    const QString user = parser.value(userOpt);
    const QString pass = parser.value(passOpt);
    const bool useSsl = parser.isSet(sslOpt);
    if (host.isEmpty() || port == 0 || user.isEmpty() || pass.isEmpty()) {
        std::cerr << "Host, port, username or password is not specified" << std::endl;
        return 1;
    }

    std::cout << "Creating KPOP3 Session" << std::endl;
    QScopedPointer<KPOP3::Connection> conn(useSsl ? new KPOP3::Connection(host, port, QSsl::AnyProtocol)
                                                  : new KPOP3::Connection(host, port));
    QEventLoop eventLoop;

    auto login = conn->login(user, pass, KPOP3::LOGIN);
    QObject::connect(login, &KJob::result,
                     [&eventLoop]() {
                        eventLoop.quit();
                     });
    eventLoop.exec();

    if (login->error()) {
        std::cerr << "Authentication failed: " << login->errorString().toStdString() << std::endl;
        return -1;
    } else {
        std::cout << "Authenticated." << std::endl;
    }

    return 0;
}
